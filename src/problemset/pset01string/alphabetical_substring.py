'''
Created on Sep 10, 2014

@author: khuong
'''

#Test Case
s ="azcbobobegghakl"

currentSubstring = "" + s[0]
maxAlphabetSubstring = currentSubstring

for i in range(len(s)-1):
    if (s[i] <= s[i+1]):
        currentSubstring = currentSubstring + s[i+1]
    else:
        if len(currentSubstring) > len(maxAlphabetSubstring):
            maxAlphabetSubstring = currentSubstring
        currentSubstring = s[i+1]
if len(currentSubstring) > len(maxAlphabetSubstring):
    print "Longest substring in alphabetical order is: " + currentSubstring
else:
    print "Longest substring in alphabetical order is: " + maxAlphabetSubstring