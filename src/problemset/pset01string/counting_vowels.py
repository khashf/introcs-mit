'''
Created on Sep 10, 2014

@author: khuong
'''
# Test case
s = "azcbobobegghakl"

i = 0
for character in s:
    if (character == 'a' or character == 'e' or character == 'i' or character == 'o' or character == 'u'):
        i += 1
print "Number of vowels: " + str(i)