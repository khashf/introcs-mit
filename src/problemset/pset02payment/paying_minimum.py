'''
Created on Sep 8, 2014

@author: khuong
'''
#Constant
NUM_OF_MONTH = 12.0

# Given variables
balance = 4842
annualInterestRate = 0.2
monthlyPaymentRate = 0.04

# Output results
monthlyInterestRate = annualInterestRate / NUM_OF_MONTH
monthlyMinimumPayment = None
monthlyUnpaidBalance = None
remainBalance = balance
totalPaid = 0

# Calculate result in every month in year
for month in range(1, 13):
    # Calculate payment
    monthlyMinimumPayment = remainBalance * monthlyPaymentRate
    monthlyUnpaidBalance = remainBalance - monthlyMinimumPayment
    #Update total paid
    totalPaid += monthlyMinimumPayment
    # Update remainBalance each month
    remainBalance = monthlyUnpaidBalance + (monthlyInterestRate * monthlyUnpaidBalance)
    print "Month: " + str(month)
    print "Minimum monthly payment: " + str(round(monthlyMinimumPayment, 2))
    print "Remaining balance: " + str(round(remainBalance, 2))
print "Total paid: " + str(round(totalPaid, 2))
print "Remaining balance: " + str(round(remainBalance, 2))


